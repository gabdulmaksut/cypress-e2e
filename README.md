# Cypress e2e example

Пример кода е2е автотестов, написанных на Cypress+TypeScript.
 В этом примере указаны неверные адреса и env переменные

## Установка

[Документация](https://docs.cypress.io/guides/getting-started/installing-cypress.html#System-requirements) 

```bash
npm install cypress --save-dev
```

## Использование

```
npm install && cypress open --env configFile=local
npm install && cypress open --env configFile=development
npm install && cypress open --env configFile=staging
npm install && cypress open --env configFile=production
```