import { randomString } from '../../support/commands';
import { watchFile } from 'fs-extra';

let serviceId = Cypress.env('serviceId');
const container1 = `${randomString()}-c1`;
const user1 = `${serviceId}_user`;

function checkMethod(methods: string[]) {
  cy.get('[data-cy="checkbox-corse-container"]')
    .find('[type="checkbox"]')
    .check(methods, { force: true });
}

function uncheckMethod(methods: string[]) {
  cy.get('[data-cy="checkbox-corse-container"]')
    .find('[type="checkbox"]')
    .check(methods, { force: true });
}

function checkHttpMethods(methods: string[], output: string, checkbox: string) {
  cy.get('.anticon.anticon-bars').click({ force: true });
  if (checkbox == 'check') {
    checkMethod(methods);
  } else {
    uncheckMethod(methods);
  }
  cy.get('[data-cy="btn-submit-corse-container"]')
    .last()
    .click();
  cy.get('[data-cy="main-cors-container"]')
    .find('.drawer-menu__name')
    .should('contain', output);
}

function checkWarningBucketName(bucketname: string, message: string) {
  cy.get('[data-cy=text-create-container-name]')
    .clear()
    .type(bucketname);
  cy.get('[data-cy="form-create-container"]')
    .find('.ant-form-explain')
    .should('contain', message);
}

function checkReviewData(object: string) {
  cy.visit(`/${serviceId}`);
  cy.get(`[data-cy="${object}-card"]`)
    .find('.num-delim')
    .invoke('text')
    .then((number) => {
      // cy.visit(`/${serviceId}/${object}`);
      cy.get(`[data-cy="${object}-card"]`)
        .find('.ant-card-head-title a')
        .click();
      cy.url({ timeout: 20000 }).should('include', object);
      cy.get('main tbody')
        .find('tr')
        .its('length')
        .then((size) => {
          expect(number).to.be.eq(size.toString());
        });
    });
}

describe('Storage Test', () => {
  before(() => {
    cy.clearCookies();
    // cy.visit(`${serviceId}/containers`);
    cy.wait(2000);
    cy.visit(`/${serviceId}`);
    cy.fillInAuthLogInForm(Cypress.env('emailU'), Cypress.env('passwordU'));
    cy.url().should('include', `/${serviceId}`);
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('connect.sid', 'connect.sid-dev');
  });

  after(() => {
    cy.deleteContainer(container1, `${serviceId}`);
  });

  describe('Обзор', () => {
    afterEach(() => {
      cy.reload();
    });

    describe('Изменение названия проекта', () => {
      it('Вывод предупреждений', () => {
        cy.get('.ant-typography.project-name')
          .find('.anticon-edit')
          .click();
        cy.get('#serviceName').clear();
        cy.get('.ant-form-item-control.has-error')
          .find('.ant-form-explain')
          .should('have.text', 'Пожалуйста введите новое название');
        cy.get('[data-cy="btn-confirm-change-project-name"]').click();
        cy.get('.ant-drawer-content-wrapper').should('be.visible');

        cy.get('#serviceName').type(
          '1111111111222222222233333333334444444444555555555566666666667777'
        );
        cy.get('.ant-row.ant-form-item').should('not.have.class', 'ant-form-item-with-help');
        cy.get('#serviceName').type('8');
        cy.get('.ant-row.ant-form-item').should('have.class', 'ant-form-item-with-help');
        cy.get('.ant-form-item-control.has-error')
          .find('.ant-form-explain')
          .should('have.text', 'Масимальное количество символов - 64');
        cy.get('[data-cy="btn-confirm-change-project-name"]').click();
        cy.get('.ant-drawer-content-wrapper').should('be.visible');

        cy.get('#serviceName')
          .clear()
          .type('проектЧ№?():)%:(0')
          .should('have.value', 'проектЧ№?():)%:(0');
        cy.get('[data-cy="btn-confirm-change-project-name"]').click();
        cy.get('.ant-typography.project-name').should('have.text', 'проектЧ№?():)%:(0');
      });

      it('Успешное переименование', () => {
        cy.get('.ant-typography.project-name')
          .find('.anticon-edit')
          .click();
        cy.get('#serviceName')
          .clear()
          .type('pooproject')
          .should('have.value', 'pooproject');
        cy.get('[data-cy="btn-confirm-change-project-name"]').click();
        cy.get('.ant-typography.project-name').should('have.text', 'pooproject');
      });
    });

    describe('Проверка карточек', () => {
      it('Количество контейнеров', () => {
        checkReviewData('containers');
      });

      it('Количество юзеров', () => {
        checkReviewData('users');
      });

      it('Количество объектов', () => {
        cy.visit(`/${serviceId}`);
        cy.get(`[data-cy="objects-card"]`)
          .find('.num-delim')
          .invoke('text')
          .then((numberObjects) => {
            cy.get(`[data-cy="objects-card"]`)
              .find('.ant-card-head-title a')
              .click();
            cy.url({ timeout: 20000 }).should('include', '/containers');
            let sum = 0;
            function getSum() {
              return new Cypress.Promise((resolve) => {
                cy.get('main tbody')
                  .find('tr td.num-objects')
                  .then((col) => {
                    cy.wrap(col).each((numObjects, index) => {
                      cy.wrap(numObjects)
                        .invoke('text')
                        .then((numberContainers) => {
                          sum += Number(numberContainers);
                          if (index === col.length - 1) {
                            resolve(sum);
                          }
                        });
                    });
                  });
              });
            }

            getSum().then((sum) => {
              expect(Number(numberObjects)).to.eq(sum);
            });
          });
      });
    });
  });

  describe('Контейнеры', () => {
    afterEach(() => {
      cy.reload();
    });

    it('Вывод предупреждений в поле названия контейнера', () => {
      cy.get('[data-cy=btn-main-create-container]').click();
      checkWarningBucketName('aa', 'Минимальное количество символов - 3.');
      checkWarningBucketName(
        '1111111111aaaaaaaaaa1111111111aaaaaaaaaa1111111111aaaaaaaaaa1111',
        'Максимальное количество символов - 63.'
      );
      checkWarningBucketName('aaa!', 'Имя контейнера должно содержать латинсикие буквы, цифры');
      checkWarningBucketName('ббб', 'Имя контейнера должно содержать латинсикие буквы, цифры');
    });

    it('Создание контейнера container1', () => {
      cy.get('[data-cy=btn-main-create-container]').click();
      cy.get('[data-cy=text-create-container-name]').type(container1);
      cy.get('[data-cy=text-create-container-comment]').type('comment');
      cy.get('[data-cy=form-create-container]').submit();
      cy.get('td div a').should('contain', container1);
    });

    it('Информация о контейнере', () => {
      cy.pressMenuButton(container1, '[data-cy="btn-info-container"]');
      cy.get('[data-cy="main-info-container"]').then((drawer) => {
        cy.wrap(drawer)
          .find('.container-info__name')
          .should('contain', container1);
        cy.wrap(drawer)
          .find('[data-row-key="4"]')
          .should('contain', `${container1}.${Cypress.env('enpointPublicUrl')}`);
        cy.wrap(drawer)
          .find('[data-row-key="5"]')
          .should('contain', 'comment');
        cy.wrap(drawer)
          .find('.ant-drawer-close')
          .click();
      });
    });

    describe('Права доступа', () => {
      beforeEach(() => {
        cy.pressMenuButton(container1, '[data-cy="btn-access-container"]');
      });

      it('Текущий пользователь', () => {
        cy.checkAccessRight(serviceId, '[value="READ_ACP"]', 'Чтение ACL');
        cy.checkAccessRight(serviceId, '[value="WRITE_ACP"]', 'Изменение ACL');
        cy.checkAccessRight(serviceId, '[value="READ"]', 'Чтение');
        cy.checkAccessRight(serviceId, '[value="WRITE"]', 'Запись');
        cy.get(`[data-cy="btn-open-${serviceId}-access-container"]`).click({ force: true });
        cy.get('[type="checkbox"]')
          .last()
          .check({ force: true })
          .should('be.checked');
        cy.contains('Применить')
          .last()
          .click({ force: true });
        cy.get('small').should('have.text', 'Полный доступ');
        cy.get('.drawer__btns-bottom .ant-btn.ant-btn-primary:visible').click();
      });

      it('Для выбранного пользователя', () => {
        cy.get('[data-cy="btn-open-main-access-container"]').click();
        cy.get('[data-cy="select-user-access-container"]').click();
        cy.contains(user1).click();
        cy.get('.ant-checkbox-input')
          .first()
          .check({ force: true });
        cy.get('[data-cy="btn-submit-general-access-container"]').click();
        cy.checkAccessRight(user1, '[value="WRITE"]', 'Запись');
        cy.checkAccessRight(user1, '[value="READ_ACP"]', 'Чтение ACL');
        cy.checkAccessRight(user1, '[value="WRITE_ACP"]', 'Полный доступ');
        cy.checkAccessRight(user1, '[value="READ_ACP"]', 'Чтение ACL');
        cy.get(`[data-cy="btn-open-${user1}-access-container"]`).click();
        cy.get('[type="checkbox"]')
          .last()
          .check({ force: true })
          .should('be.checked');
        cy.get('[data-cy="btn-submit-access-container"]').click({ force: true });

        cy.get('small')
          .eq(1)
          .should('have.text', 'Полный доступ');
      });

      it('Удаление прав пользователя', () => {
        cy.get(`[data-cy="btn-open-delete-${user1}-access-container"]`)
          .parent()
          .click();
        cy.get(`[data-cy="btn-delete-${user1}-access-container"]`).click();
      });
    });

    describe('CORS', () => {
      beforeEach(() => {
        cy.pressMenuButton(container1, '[data-cy="btn-cors-container"]');
      });

      it('Вывод предупреждений', () => {
        cy.get('[data-cy="btn-add-cors-option"]').click({ force: true });
        cy.get('[data-cy="btn-submit-corse-container"]').click({ force: true });
        cy.get('.ant-form-item-control.has-error').should('contain', 'Пожалуйста введите origin');
        cy.get('.ant-form-explain').should('contain', 'Пожалуйста выберите методы');

        cy.get('[data-cy="allow-origin-cors-container"]').type('ps.kz');
        cy.get('[data-cy="btn-submit-corse-container"]').click();
        cy.get('.ant-form-explain').should('contain', 'Пожалуйста выберите методы');
      });

      it('Добавление CORS опции', () => {
        cy.get('[data-cy="btn-add-cors-option"]').click({ force: true });
        cy.get('[data-cy="allow-origin-cors-container"]').type('ps.kz');
        cy.get('[data-cy="checkbox-corse-container"]')
          .find('[type="checkbox"]')
          .check(['GET']);
        cy.get('[data-cy="btn-submit-corse-container"]')
          .last()
          .click();
        cy.get('[data-cy="main-cors-container"]')
          .find('.drawer-menu__name')
          .should('contain', 'ps.kz')
          .and('contain', 'GET');
      });

      it('Выбор http методов', () => {
        cy.get('[data-cy="main-cors-container"]').find('.anticon.anticon-bars');
        checkHttpMethods(['GET', 'POST', 'HEAD', 'PUT'], 'GET, PUT, HEAD, POST', 'check');
        checkHttpMethods(['DELETE'], 'GET, PUT, DELETE, HEAD, POST', 'check');
        checkHttpMethods(['DELETE', 'PUT', 'GET'], 'HEAD, POST', 'uncheck');
      });

      it('Удаление CORS опции', () => {
        cy.get('[data-cy="main-cors-container"]')
          .find('.btn-delete')
          .click();
        cy.get('.ant-btn.ant-btn-danger').click({ force: true });
      });
    });

    describe('Свойства контейнера', () => {
      beforeEach(() => {
        cy.pressMenuButton(container1, '[data-cy="btn-settings-container"]');
      });

      describe('Тип контейнера', () => {
        it('Изменение типа контейнера', () => {
          cy.changeContainerType('public-read');
          cy.changeContainerType('public-read-write');
          cy.changeContainerType('private');
        });
      });

      describe('Владелец', () => {
        beforeEach(() => {
          cy.contains('Владелец')
            .parent()
            .click();
          cy.get('.ant-select-selection__rendered').click();
        });

        it(`Смена владельца на ${user1}`, () => {
          cy.get('li')
            .contains(user1)
            .click();
          cy.get('[data-cy="btn-submit-owner-container"]').click();
          cy.get('.drawer-menu__name')
            .contains('Владелец')
            .parent()
            .get('small', { timeout: 10000 })
            .should((small) => {
              expect(small).to.contain(user1);
            });
        });

        it(`Смена владельца на ${serviceId}`, () => {
          cy.get(
            '.ant-select-dropdown-menu.ant-select-dropdown-menu-root.ant-select-dropdown-menu-vertical'
          )
            .contains(`${serviceId}`)
            .click();
          cy.get('[data-cy="btn-submit-owner-container"]').click();
          cy.contains('Владелец')
            .parent()
            .find('small')
            .should('contain', `${serviceId}`);
        });
      });

      describe.skip('Время жизни объектов', () => {
        beforeEach(() => {
          cy.contains('Время жизни объектов')
            .parent()
            .click();
        });

        it('Смена периода через кнопки', () => {
          for (let i = 0; i < 10; i++) {
            cy.get('.ant-input-number-handler-up').click();
            cy.get('[data-cy="number-lifecycle-container"]').should(
              'have.value',
              (i + 2).toString()
            );
          }

          for (let i = 0; i < 10; i++) {
            cy.get('.ant-input-number-handler-down-inner').click();
            cy.get('[data-cy="number-lifecycle-container"]').should(
              'have.value',
              (11 - (i + 1)).toString()
            );
          }
          cy.get('.ant-input-number-handler-down-disabled').should(
            'have.css',
            'cursor',
            'not-allowed'
          );
          cy.get('[data-cy="btn-submit-lifecycle-container"]').click();
        });

        it('Вввод периода вручную', () => {
          cy.get('[data-cy="switch-lifecycle-container"]')
            .click()
            .should('have.class', 'ant-switch-checked');
          cy.get('[data-cy="number-lifecycle-container"]')
            .clear()
            .type('5')
            .should('have.value', '5');
          cy.get('[data-cy="btn-submit-lifecycle-container"]').click();
          cy.contains('Время жизни объектов')
            .parent()
            .find('small')
            .should('contain', '5 дней');
        });

        it('Выбор года', () => {
          cy.get('[data-cy="number-lifecycle-container"]')
            .clear()
            .type('3')
            .should('have.value', '3');
          cy.get('.ant-radio-button-wrapper')
            .contains('Года')
            .click();
          cy.get('[data-cy="btn-submit-lifecycle-container"]').click();
          cy.contains('Время жизни объектов')
            .parent()
            .find('small')
            .should('contain', '3 года');
          cy.contains('Время жизни объектов')
            .parent()
            .click();
          cy.get('[data-cy="number-lifecycle-container"]').should('have.value', '1095');
          cy.get('[data-cy="day-lifecycle-container"]')
            .parent()
            .should('have.class', 'ant-radio-button-checked');
        });

        it('off', () => {
          cy.get('[data-cy="number-lifecycle-container"]')
            .clear()
            .type('0')
            .should('have.value', '0');

          cy.get('[data-cy="switch-lifecycle-container"]')
            .click()
            .should('not.have.class', 'ant-switch-checked');
          cy.get('[data-cy="btn-submit-lifecycle-container"]').click();
        });
      });

      describe('Вeрсионирование', () => {
        beforeEach(() => {
          cy.contains('Версионирование')
            .parent()
            .click();
        });

        it('Включить версионирование', () => {
          cy.get('[data-cy="switch-versioning-container"]').then((virsionPanel) => {
            cy.wrap(virsionPanel)
              .parent()
              .should('contain', 'Выключено');
            cy.wrap(virsionPanel)
              .click()
              .should('have.class', 'ant-switch-checked');
            cy.get('[data-cy="btn-submit-versioning-container"]').click();
            cy.wrap(virsionPanel)
              .parent()
              .should('contain', 'Включено');
          });
        });

        it('Выключить версионирование', () => {
          cy.get('[data-cy="switch-versioning-container"]').then((virsionPanel) => {
            cy.wrap(virsionPanel)
              .parent()
              .should('contain', 'Включено');
            cy.wrap(virsionPanel)
              .click()
              .should('not.have.class', 'ant-switch-checked');
            cy.get('[data-cy="btn-submit-versioning-container"]').click();
            cy.wrap(virsionPanel)
              .parent()
              .should('contain', 'Выключено');
          });
        });
      });
    });

    describe('Удаление контейнера container1', () => {
      it('Удалить', () => {
        cy.pressMenuButton(container1, '[data-cy="btn-settings-container"]');
        cy.get('.drawer-menu__item.drawer-menu__item-delete').click();
        cy.get('[data-cy="delete-container"]')
          .find('#bucketName:visible')
          .type(container1, { force: true });
        cy.get('.ant-form.ant-form-horizontal:visible').submit();
        cy.get('main')
          .find('.ant-table-wrapper.project-table')
          .should('not.contain', container1);
      });
    });
  });
});
