import 'cypress-file-upload';
import find = require('cypress/types/lodash/find');
import { watchFile } from 'fs-extra';
export {};

function randomString() {
  return Math.random()
    .toString(36)
    .substring(3);
}

const objectContainer = randomString();
const fileName1 = 'testFile1.jpeg';
const fileName2 = 'testFile2.jpeg';
const fileName3 = 'testFile3.jpeg';
const fileName4 = 'testFile4.jpeg';
const fileName5 = 'testFile5.jpeg';
const catalog1 = 'catalog1'; //for copy
const catalog2 = 'catalog2'; //for move
let serviceId = Cypress.env('serviceId');

function findFile(fileName: string, catalog: string) {
  cy.get(`[data-row-key="${fileName}"]`)
    .find('a')
    .should(
      'have.attr',
      'href',
      `https://${objectContainer}.${Cypress.env('enpointPublicUrl')}/${catalog}${fileName}`
    );
}

function fileUpload(fileName: string) {
  cy.fixture(`../testfiles/${fileName}`).then((fileContent) => {
    cy.get('label.btn-upload.ant-btn')
      .find('input[type=file]')
      .upload({
        fileContent,
        fileName,
        mimeType: 'image/png',
      });
  });
  cy.wait(2000);
}

function changeFileType(fileName: string, type: string, icon: string) {
  cy.get(`[data-row-key="${fileName}"]`)
    .find('.anticon.anticon-setting')
    .click({ force: true });
  cy.get('#acl')
    .find('.ant-select-selection-selected-value')
    .click();
  cy.get('.ant-select-dropdown-menu')
    .contains(type)
    .click();
  cy.get('[data-cy="btn-confirm-change-file-type"]').click();
  cy.wait(1000);
  cy.get(`[data-row-key="${fileName}"]`)
    .find('i')
    .should('have.class', `anticon-${icon}`);
}

function createCatalog(catalogName: string) {
  cy.url().should('include', objectContainer);
  cy.get('.ant-btn.ant-btn-primary')
    .contains('Создать каталог')
    .click({ force: true });
  cy.get('input#key.ant-input')
    .type(catalogName)
    .should('have.value', catalogName);
  cy.get('[data-cy="btn-confirm-create-catalog"]').click();
  cy.wait(2000);
  cy.get('.ant-table-row.ant-table-row-level-0').should('contain', catalogName);
}

function selectElement(element: string) {
  cy.get(`[data-row-key="${element}"]`)
    .find('input[type=checkbox]')
    .check();
}

describe('Storage Test', () => {
  before(() => {
    cy.clearCookies();
    // cy.visit(`/${serviceId}`);
    cy.visit(`${serviceId}/containers`);
    cy.fillInAuthLogInForm(Cypress.env('emailU'), Cypress.env('passwordU'));
    cy.url().should('include', `/${serviceId}`);
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('connect.sid', 'connect.sid-dev');
  });

  after(() => {
    // cy.deleteContainer(objectContainer, `${serviceId}`);
  });

  it('create bucket', () => {
    cy.wait(5000);
    cy.createBucket(objectContainer);
    cy.reload();
    cy.get('.container-name')
      .contains(objectContainer)
      .click();
  });

  describe('Каталог', () => {
    it('Создание каталога', () => {
      createCatalog(catalog1);
      createCatalog(catalog2);
    });

    it('Проверка калькулятора', () => {
      cy.get('[data-row-key="catalog1"]')
        .find('.anticon-calculator')
        .click();
      cy.get('[data-row-key="catalog1"]')
        .find('.table-control')
        .should('contain', '0 Б');
    });
  });

  describe('Файлы', () => {
    beforeEach(() => {
      cy.reload();
    });

    it('Загрузка файлов', () => {
      fileUpload(fileName1);
      fileUpload(fileName2);
      fileUpload(fileName3);
    });

    it('Проверка ссылки файлов', () => {
      findFile(fileName1, '');
      findFile(fileName2, '');
      findFile(fileName3, '');
    });

    it('Копирование ссылки', () => {
      cy.get(`[data-row-key="${fileName1}"]`)
        .find('.table-control .anticon-share-alt')
        .click({ force: true });
      cy.get('.ant-notification-notice-message').should('contain', 'Ссылка на объект скопирована');
      // cy.get('.ant-input-search.ant-input-search-enter-button.ant-input-group-wrapper')
      //   .find('input[type=text]')
      //   .rightclick();
      // cy.wait(2000);
      // .invoke('val')
      // .then((input) => {
      //   expect(input).to.contain(`https://${objectContainer}.${Cypress.env('enpointPublicUrl')}`);
      // });
    });

    it('Изменить тип файла', () => {
      changeFileType(fileName1, 'Публичный', 'global');
      changeFileType(fileName1, 'Приватный', 'lock');
    });

    it('Иземение типа для нескольких файлов', () => {
      selectElement(fileName1);
      selectElement(fileName2);
      selectElement(fileName3);
      cy.contains('Изменить тип').click();
      cy.get('#acl').click();
      cy.get('.ant-select-dropdown-menu')
        .contains('Публичный')
        .click();
      cy.get('[data-cy="btn-confirm-change-object-type"]')
        .contains('Применить')
        .click({ force: true });
      cy.wait(1000);

      cy.get(`[data-row-key="${fileName1}"]`)
        .find('i')
        .should('have.class', `anticon-global`);
      cy.get(`[data-row-key="${fileName2}"]`)
        .find('i')
        .should('have.class', `anticon-global`);
      cy.get(`[data-row-key="${fileName3}"]`)
        .find('i')
        .should('have.class', `anticon-global`);
    });
    it('Копирование файлов', () => {
      selectElement(fileName1);
      selectElement(fileName2);
      cy.get('[data-cy="btn-copy-objects"]').click();
      cy.get('.ant-table-row-level-0')
        .find('.ant-table-row-expand-icon')
        .click();
      cy.get('[data-cy="form-copy-objects"]')
        .find(`[data-row-key=${catalog1}]`)
        .click();
      cy.get('[data-cy="btn-confirm-copy-objects"]').click();
      cy.wait(1000);
      findFile(fileName1, '');
      findFile(fileName2, '');
      cy.get('#objects-list')
        .find(`[data-row-key=${catalog1}]`)
        .click();
      cy.get('.ant-input-search')
        .find('input[type=text]')
        .should('have.value', `${catalog1}/`);
      findFile(fileName1, `${catalog1}/`);
      findFile(fileName2, `${catalog1}/`);
      cy.get('.ant-table-row-level-0')
        .find('.anticon-enter')
        .click();
    });

    it('Перемещение файлов', () => {
      cy.get('.ant-input-search')
        .find('input[type=text]')
        .clear();
      cy.get('.ant-input-search-button').click();
      selectElement(fileName1);
      selectElement(fileName2);
      cy.get('[data-cy="btn-move-objects"]').click();
      cy.get('.ant-table-row-level-0')
        .find('.ant-table-row-expand-icon')
        .click();
      cy.get('.ant-form-horizontal')
        .find('.ant-table-tbody')
        .find(`[data-row-key=${catalog2}]`)
        .click();
      cy.get('[data-cy="btn-confirm-move-objects"]').click();
      cy.wait(2000);
      cy.get('#objects-list')
        .find(`[data-row-key=${catalog2}]`)
        .click();
      cy.get('.ant-input-search')
        .find('input[type=text]')
        .should('have.value', `${catalog2}/`);
      findFile(fileName1, `${catalog2}/`);
      findFile(fileName2, `${catalog2}/`);
      cy.get('.ant-table-row-level-0')
        .find('.anticon-enter')
        .click();
      cy.get('.ant-table-tbody')
        .should('not.contain', fileName1)
        .and('not.contain', fileName2);
    });

    it('Удаление одного файла', () => {
      cy.get(`[data-row-key="${fileName3}"]`)
        .find('.anticon-delete')
        .click();
      cy.get('[data-cy="btn-confirm-delete-object"]', { timeout: 3000 }).click();
      cy.get('.ant-table-tbody').should('not.contain', fileName3);
    });

    it('Удаление нескольких файлов', () => {
      fileUpload(fileName2);
      fileUpload(fileName3);
      selectElement(fileName2);
      selectElement(fileName3);
      cy.get('[data-cy="btn-delete-objects"]').click();
      cy.get('.ant-drawer-body')
        .should('contain', fileName2)
        .and('contain', fileName3);
      cy.get('[data-cy="btn-confirm-delete-objects"]').click();
      cy.get('.ant-table-tbody')
        .should('not.contain', fileName2)
        .and('not.contain', fileName3);
    });
  });
});
