import { randomString } from '../../support/commands';

let serviceId = Cypress.env('serviceId');
const containerForBob = `${randomString()}-cb`;
const containerForBob2 = `${randomString()}-cb2`;
const bob = `${randomString()}-b`;
const bobWithContainers = `${randomString()}-bc`;
const bobWithContainers2 = `${randomString()}-bc2`;

describe('Storage Test', () => {
  before(() => {
    cy.clearCookies();
    cy.visit(`/${serviceId}`);
    cy.fillInAuthLogInForm(Cypress.env('emailU'), Cypress.env('passwordU'));
    cy.wait(1000);
    cy.url().should('include', `/${serviceId}`);
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce('connect.sid', 'connect.sid-dev');
  });

  after(() => {
    cy.deleteContainer(containerForBob, `${serviceId}_${bobWithContainers}`);
    cy.deleteContainer(containerForBob, `${serviceId}`);
    cy.deleteContainer(containerForBob2, serviceId);
    cy.deleteUser(`${serviceId}_${bob}`);
    cy.deleteUser(`${serviceId}_${bobWithContainers}`);
  });

  describe('Пользователи', () => {
    afterEach(() => {
      cy.reload();
    });

    it('Переход на "Пользователи"', () => {
      cy.get('a')
        .contains('Пользователи')
        .click();
      cy.wait(3000);
    });

    describe('Создание нового пользователя', () => {
      before(() => {
        cy.visit(`${serviceId}/users`);
      });

      it('Создание нового пользователя без контейнеров', () => {
        cy.createUser(bob);
      });

      it('Создание пользователя с контейнером', () => {
        cy.createUser(bobWithContainers);
        cy.get('a');
        cy.createContainer(containerForBob, `${serviceId}_${bobWithContainers}`);
      });
    });

    describe('Удаление пользователя', () => {
      before(() => {
        cy.visit(`/${serviceId}/users`);
      });

      it('Отмена удаления', () => {
        cy.pressMenuButton(`${serviceId}_${bob}`, '.anticon.anticon-delete');
        cy.get(`[data-cy="${serviceId}_${bob}-input-users-delete-container"]`)
          .type(`${serviceId}_${bob}`)
          .should('have.value', `${serviceId}_${bob}`);
        cy.get(`[data-cy="${serviceId}_${bob}-btn-cancel-users-delete-container"]`).click(); //ref
      });

      it('Удаление пользователя без контейнеров', () => {
        cy.pressMenuButton(`${serviceId}_${bob}`, '.anticon.anticon-delete');
        cy.get(`[data-cy="${serviceId}_${bob}-input-users-delete-container"]`) //ref _${bob}-input-users-delete-container
          .type(`${serviceId}_${bob}`)
          .should('have.value', `${serviceId}_${bob}`);
        cy.get(`[data-cy="${serviceId}_${bob}-btn-delete-users-delete-container"]`).click();
        cy.wait(1000);
        cy.get('main')
          .find('.ant-table-wrapper.project-table')
          .should('not.contain', `${serviceId}_${bob}`);
      });

      describe('Удаление пользователя с контейнером', () => {
        it('Удаление вместе с контейнером', () => {
          cy.pressMenuButton(`${serviceId}_${bobWithContainers}`, '.anticon.anticon-delete');
          cy.get('[type="checkbox"]').check([containerForBob]);
          cy.get(
            `[data-cy="${serviceId}_${bobWithContainers}-delete-containers-user-delete-container"]`
          ).click();

          cy.get(`[data-cy="${serviceId}_${bobWithContainers}-input-users-delete-container"]`) //ref
            .type(`${serviceId}_${bobWithContainers}`, { force: true })
            .should('have.value', `${serviceId}_${bobWithContainers}`);
          cy.get(
            `[data-cy="${serviceId}_${bobWithContainers}-btn-delete-users-delete-container"]`
          ).click({ force: true });
          cy.get('main')
            .find('.ant-table-wrapper.project-table')
            .should('not.contain', `${serviceId}_${bobWithContainers}`);
        });

        it('Удаление с передачей прав', () => {
          cy.createUser(bobWithContainers2);
          cy.createContainer(containerForBob2, `${serviceId}_${bobWithContainers2}`);

          cy.visit(`/${serviceId}/users`);
          cy.pressMenuButton(`${serviceId}_${bobWithContainers2}`, '.anticon.anticon-delete');
          cy.get('[type="checkbox"]').check([containerForBob2]);
          cy.get(
            `[data-cy="${serviceId}_${bobWithContainers2}-change-owner-btn-user-delete-container"]`
          ).click();

          cy.get(`[data-cy="${serviceId}_${bobWithContainers2}-input-users-delete-container"]`)
            .type(`${serviceId}_${bobWithContainers2}`, { force: true })
            .should('have.value', `${serviceId}_${bobWithContainers2}`);
          cy.get(
            `[data-cy="${serviceId}_${bobWithContainers2}-btn-delete-users-delete-container"]`
          ).click();
          cy.get('main')
            .find('.ant-table-wrapper.project-table')
            .should('not.contain', `${serviceId}_${bobWithContainers2}`);
          cy.visit(`/${serviceId}/containers`);
          cy.pressMenuButton(containerForBob2, '[data-cy="btn-access-container"]');
          cy.get('[data-cy="main-access-container"]')
            .find('.drawer-menu__name')
            .should('contain', serviceId)
            .and('contain', 'Полный доступ');
        });
      });
    });
  });
});
