import { Promise } from 'cypress/types/bluebird';
declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Cypress {
    interface Chainable {
      loginRequest: (email: string, password: string) => void;
      checkAccessRight: (user: string, value: string, text: string) => void;
      pressMenuButton: (row: string, button: string) => void;
      deleteContainer: (bucket: string, user: string) => void;
      deleteUser: (user: string) => void;
      createUser: (user: string) => void;
      createContainer: (bucket: string, user: string) => void;
      createBucket: (bucketMName: string) => void;
      changeContainerType: (type: string) => void;
    }
  }
}
export function randomString(): string {
  return (
    'cypress-' +
    Math.random()
      .toString(36)
      .substring(3)
  );
}

const loginQuery = `
mutation($username:String! $password:String! $remember:Boolean) {
  guest {
    id {
      login(username:$username password:$password remember:$remember) {
        ...on QueryUserId {
          isAuth
        }
        ...on IdTwoFactor {
          twoFactor
        }
        ...on UserNotFound {
          notFound
        }
      }
    }
  }
}
`;

export function loginRequest(email: string, password: string) {
  cy.request({
    method: 'POST',
    url: Cypress.env('authGraphQlUrl'),
    body: {
      query: loginQuery,
      variables: {
        username: email,
        password,
      },
    },
  }).then((resp) => {
    console.log(resp);
  });
  cy.getCookie(Cypress.env('connectCidCookie')).should('exist');
}

export function checkAccessRight(user: string, value: string, text: string) {
  const submitButt = 'btn-submit-access-container';

  cy.get('[data-cy="' + `btn-open-${user}-access-container` + '"]').click();
  cy.get('.ant-checkbox', { timeout: 5000 })
    .find(value)
    .then((checkbox) => {
      cy.wrap(checkbox)
        .parent()
        .then((checkboxDiv) => {
          if (checkboxDiv.hasClass('ant-checkbox-checked')) {
            cy.wrap(checkboxDiv)
              .find('.ant-checkbox-input')
              .uncheck({ force: true })
              .should('not.be.checked');

            let countChecked = 0;
            cy.get('.ant-checkbox', { timeout: 500 })
              .each((el) => {
                if (el.hasClass('ant-checkbox-checked')) {
                  countChecked++;
                }
              })
              .then(() => {
                if (countChecked == 0) {
                  cy.get('.ant-form-explain').should('contain.text', 'Пожалуйста выберите доступ');
                  cy.wrap(checkboxDiv)
                    .find('.ant-checkbox-input')
                    .check({ force: true })
                    .should('be.checked');

                  cy.get('[data-cy="' + submitButt + '"]').click();
                  cy.wait(1500);
                  cy.get('[data-cy="main-access-container"]')
                    .contains(user)
                    .parent()
                    .find('small')
                    .should('contain', text);
                } else {
                  cy.get('[data-cy="' + submitButt + '"]').click();
                  cy.wait(1500);
                  cy.get('[data-cy="main-access-container"]')
                    .contains(user)
                    .parent()
                    .find('small')
                    .should('not.contain', text, 5);
                }
              });
          } else {
            cy.wrap(checkboxDiv)
              .find('.ant-checkbox-input')
              .check({ force: true })
              .should('be.checked');

            cy.get('[data-cy="' + submitButt + '"]').click();
            cy.wait(1500);
            cy.get('[data-cy="main-access-container"]')
              .contains(user)
              .parent()
              .find('small')
              .should('contain', text);
          }
        });
    });
}
export function pressMenuButton(row: string, button: string) {
  cy.get(`[data-cy=${row}]`)
    .find(button)
    .click();
}

const userIdsQuery = `
query userId($accountId: String!, $serviceId: String!){
  Storage{
    Service{
      listServiceUsers(input:{
        accountId:$accountId, serviceId:$serviceId
      }){
        displayName
        id
      }
      }
    }
  }
`;

function getUserId(userName: string) {
  return new Cypress.Promise((resolve) => {
    cy.request({
      method: 'POST',
      url: Cypress.env('graphqlUrl'),
      body: {
        query: userIdsQuery,
        variables: {
          serviceId: Cypress.env('serviceId'),
          accountId: Cypress.env('accountId'),
        },
      },
    }).then((resp) => {
      const users =
        resp.allRequestResponses[0]['Response Body'].data.Storage.Service.listServiceUsers;
      const user = users.find((item) => item.displayName === userName);
      if (user) {
        resolve(user.id);
      }
    });
  });
}
const deleteContainerQuery = `
mutation($serviceId:String! $bucketName:String! $originBucketName:String! $userId:String!){
  Storage{
    Me{
      Bucket{
        deleteBucket(input:{
          serviceId:$serviceId bucketName:$bucketName originBucketName:$originBucketName userId:$userId})
      }
    }
  }
}
`;
export function deleteContainer(bucket: string, user: string) {
  getUserId(user)
    .then((data) => {
      cy.request({
        method: 'POST',
        url: Cypress.env('graphqlUrl'),
        body: {
          query: deleteContainerQuery,
          variables: {
            serviceId: Cypress.env('serviceId'),
            bucketName: bucket,
            originBucketName: bucket,
            userId: data,
          },
        },
      });
    })
    .catch((e) => console.log(e));
}

const deleteUserQuery = `
mutation deleteUser($serviceId:String! $userId:String!){
  Storage{
    Me{
      Service{
        deleteUser(input:{
          serviceId:$serviceId userId:$userId})
      }
    }
  }
}
`;

export function deleteUser(user: string) {
  getUserId(user)
    .then((data) => {
      cy.request({
        method: 'POST',
        url: Cypress.env('graphqlUrl'),
        body: {
          query: deleteUserQuery,
          variables: {
            serviceId: Cypress.env('serviceId'),
            userId: data,
          },
        },
      });
    })
    .catch((e) => console.log(e));
}

export function createUser(user: string) {
  cy.get('[data-cy="btn-add-new-user"]').click();
  cy.get('[data-cy="new-user-name-input"]').then((userInput) => {
    cy.wrap(userInput)
      .type(user)
      .should('have.value', user)
      .clear();
    cy.get('.ant-form-explain').should('have.text', 'Пожалуйста введите имя пользователя');
    cy.wrap(userInput)
      .type(user)
      .should('have.value', user);
  });
  cy.get('[data-cy="new-user-name-comment"]')
    .type('comment')
    .should('have.value', 'comment');
  cy.get('[data-cy="btn-create-new-user"]').click();
  cy.wait(1000);
}

export function createContainer(bucket: string, user: string) {
  cy.visit(`/${Cypress.env('serviceId')}/containers`);
  cy.get('[data-cy=btn-main-create-container]').click();
  cy.get('[data-cy=text-create-container-name]')
    .type(bucket)
    .should('have.value', bucket);
  cy.get('[data-cy=text-create-container-comment]').type('comment');
  cy.get('[data-cy="select-create-container-owner"]').click();
  cy.contains(user).click();
  cy.get('[data-cy=form-create-container]').submit();
  cy.get('td div a').should('contain', bucket);
}

const createBucketQuery = `
mutation createBucket($serviceId:String! $bucketAcl:acl! $bucketName:String! $comment:String! $userId:String!){
  Storage{
    Me{
      Bucket{
        createBucket(input:{
          serviceId: $serviceId
          bucketAcl: $bucketAcl
          bucketName: $bucketName
          comment: $comment
          userId: $userId
        }){
          bucketName
        }
      }
    }
  }
}
`;

export function createBucket(bucketName: string) {
  getUserId(Cypress.env('serviceId'))
    .then((userID) => {
      cy.request({
        method: 'POST',
        url: Cypress.env('graphqlUrl'),
        body: {
          query: createBucketQuery,
          variables: {
            serviceId: Cypress.env('serviceId'),
            bucketAcl: 'private',
            bucketName: bucketName,
            comment: 'comment',
            userId: userID,
          },
        },
      });
    })
    .catch((e) => console.log(e));
}

export function changeContainerType(type: string) {
  let text = '';

  switch (type) {
    case 'private':
      text = 'Приватный';
      break;
    case 'public-read':
      text = 'Публичный (чтение)';
      break;
    case 'public-read-write':
      text = 'Публичный (чтение + запись)';
      break;
  }

  cy.contains('Тип контейнера')
    .parent()
    .click();
  cy.get('.ant-select-selection-selected-value').click();
  cy.get(`[data-cy="${type}-settings-container"]`).click();
  cy.get('[data-cy="btn-submit-type-settings-container"]').click();
  cy.get('.drawer-menu__name', { timeout: 10000 })
    .contains('Тип контейнера')
    .parent()
    .find('small')
    .should((small) => {
      expect(small).to.contain(text);
    });
}

Cypress.Commands.add('checkAccessRight', checkAccessRight);
Cypress.Commands.add('pressMenuButton', pressMenuButton);
Cypress.Commands.add('deleteContainer', deleteContainer);
Cypress.Commands.add('deleteUser', deleteUser);
Cypress.Commands.add('createUser', createUser);
Cypress.Commands.add('createContainer', createContainer);
Cypress.Commands.add('createBucket', createBucket);
Cypress.Commands.add('loginRequest', loginRequest);
Cypress.Commands.add('changeContainerType', changeContainerType);
