declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Cypress {
    interface Chainable {
      login: (email: string, password: string) => void;
      logout: () => void;
      fillInAuthLogInForm: (email: string, password: string) => void;
      fillIn2factor: (code: string) => void;
    }
  }
}
const query = `
mutation{
  guest{
    id{
      login(username: "e2e-client-u@ps.kz", password: "12345678" , remember: true){
        ...on QueryUserId {
          isAuth
          profile {
            username
          }
        }
        ...on IdTwoFactor {
          twoFactor
        }
        ...on UserNotFound {
          notFound
        }
        __typename
      }
  }
  }
}
`;

export function login(email: string, password: string) {
  cy.request({
    method: 'POST',
    url: 'https://www.ps.kz/login/proceed',
    form: true,
    body: {
      cabinet: email,
      magic: password,
      rememberme: '1',
    },
  });
  cy.getCookie('connect.sid').should('exist');
}

export function logout() {
  cy.clearCookies();
  cy.visit('/login');
}

export function fillInAuthLogInForm(email: string, password: string) {
  cy.get('#normal_login_username')
    .clear()
    .type(email)
    .should('have.value', email);
  cy.get('#normal_login_password')
    .clear()
    .type(password)
    .should('have.value', password);
  cy.get('.login-form').submit();
}

export function fillIn2factor(code: string) {
  cy.get('#normal_login_token')
    .type(code)
    .should('have.value', code);
  cy.get('.login-form').submit();
}

Cypress.Commands.add('login', login);
Cypress.Commands.add('logout', logout);
Cypress.Commands.add('fillInAuthLogInForm', fillInAuthLogInForm);
Cypress.Commands.add('fillIn2factor', fillIn2factor);
